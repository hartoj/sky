var timesOfDayArray = ["<li>Dawn</li>", "<li>Morning</li>", "<li>Noon</li>", "<li>Afternoon</li>", "<li>Evening</li>", "<li>Midnight</li>"];

$(document).ready(function() {
  var myURL = "https://api.weather.gov/stations/KLAX/observations/latest";
  $("#greetingText").css("display", "none");
  
    $.ajax({
    url: myURL,
    dataType: "json",
    success: function(data) {
      var icon = data["properties"].icon;
      var iconString = ('<li><img src="' + icon + '"></li>');
      $('#myImage').append(iconString);
    }
  });
});

function greetFunction() {
  $("#greetingText").fadeToggle(2000, function() {
    console.log("toggled greeting");
  });
}

$('h1').css({
    color: "white",
    margin: "20px",
  });

$('.container').css({
    color: "#10104a",
    padding: "20px",
  });

$('ul').append(timesOfDayArray[0]);
$('ul').append(timesOfDayArray[1]);
$('ul').append(timesOfDayArray[2]);
$('ul').append(timesOfDayArray[3]);
$('ul').append(timesOfDayArray[4]);
$('ul').append(timesOfDayArray[5]);